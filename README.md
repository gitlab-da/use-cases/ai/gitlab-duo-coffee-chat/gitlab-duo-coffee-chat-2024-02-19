# Gitlab Duo Coffee Chat: Contribute to GitLab using Code Suggestions and Chat

- Host: @dnsmichi 
- Guest: @stingrayza

## Recording

[![GitLab Duo Coffee Chat](https://img.youtube.com/vi/TauP7soXj-E/0.jpg)](https://go.gitlab.com/1xdHTo)

## Summary

Challenge: Improve the GitLab profile page social network input forms and use Chat and Code Suggestions. One full-stack engineer, one backend engineer, max. 80 minutes.

After a short introduction to the GitLab Development Kit (GDK), we found the file with the HAML template rendering the profile forms. Maybe we can create a dropdown for LinkedIn, X, and Mastodon? 

We learned that Code Suggestions can generate HAML (Ruby) code, and we explored Duo Chat code tasks with refined prompts practice: /explain the algorithms, /refactor into HAML dropdowns (multiple attempts). We also asked Chat to explain specific error messages. 

The dropdown works ... but we forgot something. There must be an input form for the social network's value, requiring a different UX approach. Excellent learning curve with the GitLab code, though. We'll explore more contribution possibilities next time. 

## Resources

In this session

- Draft MR with commit history: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145692 
- HAML: https://haml.info/ 

General

- YouTube Playlist: https://go.gitlab.com/xReaA1 
- GitLab group with source code projects: https://go.gitlab.com/uTwX11 
- GitLab Duo: https://go.gitlab.com/Z1vBGD 
- Documentation: https://go.gitlab.com/rSbrTI 
    - GitLab Duo Code Suggestions: https://go.gitlab.com/XIuZ5q 
    - GitLab Duo Chat and code tasks /explain, /refactor, /tests: https://go.gitlab.com/XLpq9a 
    - Vulnerability Resolution: https://go.gitlab.com/4jZ2Cr 
    - Root Cause Analysis: https://go.gitlab.com/2poHWF     
- Talk: Efficient DevSecOps Workflows with a little help from AI: https://go.gitlab.com/T864XF 
- Code Suggestions feedback issue: https://go.gitlab.com/07r1sv 
- Duo Chat feedback issue: https://go.gitlab.com/XTU14S 
- GitLab Community Forum: https://go.gitlab.com/GfTthQ
- GitLab Discord: https://go.gitlab.com/YLWVjM 
